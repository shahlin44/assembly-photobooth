var buttonPrint = document.getElementById("confirm_print_btn");
var select = document.getElementById("filters_list");

function change_filter(){
    var selected = select.value;

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            // Success
            // alert(xhr.responseText);
        }
    }

    xhr.open("GET", "/video_viewer?filter=" + selected);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.send(JSON.stringify({ status: "true" }));
}