import cv2

img = cv2.imread('static/images/previews/9.png')
newImage = img
i, j, k = img.shape
for x in range(i):
    for y in range(j):
     R = img[x,y,2] * 0.393 + img[x,y,1] * 0.769 + img[x,y,0] * 0.189
     G = img[x,y,2] * 0.349 + img[x,y,1] * 0.686 + img[x,y,0] * 0.168
     B = img[x,y,2] * 0.272 + img[x,y,1] * 0.534 + img[x,y,0] * 0.131
     if R > 255:
        newImage[x,y,2] = 255
     else:
        newImage[x,y,2] = R
     if G > 255:
        newImage[x,y,1] = 255
     else:
        newImage[x,y,1] = G
     if B > 255:
        newImage[x,y,0] = 255
     else:
        newImage[x,y,0] = B
       
        
cv2.imwrite('sepia.png',newImage)
# cv2.imshow('image',blur)
cv2.waitKey(0)
cv2.destroyAllWindows()