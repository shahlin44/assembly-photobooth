from server import *
import unittest

class TestServer(unittest.TestCase):
    def testLatestNum(self):
        latestNum = getNextImageNum()
        self.assertEqual(latestNum, 11)

    def testLastNum(self):
        lastNum = getLastImageNum()
        self.assertEqual(lastNum, 10)

if __name__ == '__main__':
    unittest.main()