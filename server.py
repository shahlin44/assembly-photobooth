from flask import Flask, render_template, Response, request
from camera import VideoCamera
from sendgrid.helpers.mail import *
from TwitterAPI import TwitterAPI
import sendgrid
import pymysql
import base64
import os

app = Flask(__name__)
app.debug = True

video_camera = None
global_frame = None
current_filter = None
emails_list = []

IMAGES_DIR = 'static/images/'

@app.route('/index')
def index():
    return render_template('index.html')

@app.route('/')
def details():
    return render_template('details.html')

@app.route('/live')
def live():
    live_filter = request.args.get('filter')

    if live_filter is None:
        live_filter = "stylization"
    
    return render_template('live.html', filter=live_filter)

@app.route('/validate_details', methods=['POST'])
def validate_details():
    # Use the global variable to store emails
    # So it can be used outside
    global emails_list

    emails = request.form.get('emails')
    
    # Insert comma separated emails to a list
    emails_list = [x.strip() for x in emails.split(',')]

    # Remove empty values from the list
    emails_list = [value for value in emails_list if value != ""]

    for email in emails_list:
        if validateEmail(email) is False:
            return "\"" + email + "\" is not a valid email"

    return 'Success'

@app.route('/print_img', methods=['POST', 'GET'])
def print_img(): 
    global video_camera
    
    # Save details to db
    img_location = IMAGES_DIR + "print_previews/" + str(getLastImageNum("print_previews")) + ".png"
    insertDataToDb(img_location, "print")

    video_camera.print_img(img_location)

    return 'Printing now!'

@app.route('/get_preview', methods=['POST'])
def get_preview():
    type = request.form.get('type')
    tweet_filter = request.form.get('tweet_filter')

    if type == 'email':
        # Normal image
        video_camera.save_png("previews/" + str(getNextImageNum(folder="previews")))

        # Stylized image
        video_camera.save_png("previews/" + str(getNextImageNum(folder="previews")), filter="stylized")

        # Sketch
        video_camera.save_png("previews/" + str(getNextImageNum(folder="previews")), filter="sketch")

        # Gaussian image
        video_camera.save_png("previews/" + str(getNextImageNum(folder="previews")), filter="gaussian")

        return "Picture Taken"
    else:
        return video_camera.save_png("previews/" + str(getNextImageNum(folder="previews")), filter=tweet_filter)

@app.route('/print_preview', methods=['POST'])
def print_preview():
    # Save the normal image
    video_camera.save_png(str(getNextImageNum(folder='print_previews')))

    # Save the filtered image
    video_camera.save_png("print_previews/" + str(getNextImageNum(folder='print_previews')), filter='gaussian')

    return "Success"

@app.route('/send_email', methods=['POST'])
def send_email():
    """
        Insert data into database and call sendEmail()
        to send successfully send the email
    """

    global emails_list

    additional_emails = request.form.get('additional_emails')

    # Insert comma separated emails to a list
    additional_emails_list = [x.strip() for x in additional_emails.split(',')]

    # Remove empty values from the list
    additional_emails_list = [value for value in additional_emails_list if value != ""]

    # Save details to db
    img_loc = IMAGES_DIR + "previews/" + str(getLastImageNum(folder="previews")) + ".png"

    for additional_email in additional_emails_list:
        # Check if email is invalid
        if validateEmail(additional_email) is False:
            return "\"" + additional_email + "\" is not a valid email"
        else:
            emails_list.append(additional_email)

    separated_emails = ', '.join(emails_list)

    insertDataToDb(img_loc, "email", separated_emails)

    # Send email(s) with attachment
    for email in emails_list:
        sendEmail(email)

    return 'Email sent'

@app.route('/video_viewer')
def video_viewer():
    global current_filter

    # If the filter is null (from JS) or default (Select a filter option), set filter to None
    # Else just set filter to the selected filter
    current_filter = None if (request.args.get('filter') == 'null') else request.args.get('filter') 

    # Default filter
    if current_filter is None:
        current_filter = 'default'

    return Response(video_stream(),
                    mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route('/post_twitter', methods=['POST'])
def post_twitter():
    global video_camera
    global current_filter
    global emails_list

    api = TwitterAPI("zluZLZGV1qcPk4qKImEYH7mFP", "ft2hoTzSha3LbAcvaWO6o982jfqt0pckWe3Cf7yCyafsFXigEW", "1048265445634134016-6hW1wWPYZZbQSQQwPDL1g4jUd1bmf7", "C6vsrRLBrAzibY7NLkSFbugZU1XNfpXx1MhFICsbyoCzq")
    username = request.form.get('username')

    file_path = IMAGES_DIR + "previews/" + str(getLastImageNum(folder='previews')) + ".png"
    file = open(file_path, 'rb')
    data = file.read()
    
    r = api.request('statuses/update_with_media', {'status':'Thank you for visiting the Assembly at #Gitex2018 @' + username + ' - Let\'s @MakeSmartThings together! #Innovation #GitexFutureStars2018' }, {'media[]':data})

    separated_emails = ', '.join(emails_list)
    insertDataToDb(file_path, 'tweet', separated_emails, username)

    return 'Success'

def getNextImageNum(folder=""):
    """
        Gets the number for the next image
        So if there are 8 images in the folder,
        this function will return 9
    """
    import os
    
    images_list = os.listdir(IMAGES_DIR + folder)

    # Do not count previews folder
    if folder == "":
        images_list.remove('previews')
        images_list.remove('print_previews')

    # If there are no existing images, return 1
    if len(images_list) == 0:
        return 1

    # Get the maximum number from string numbers
    max_num = int(images_list[0][:-4])
    for i in images_list:
        # First convert string into int
        img_num = int(i[:-4])

        # Compare
        if img_num > max_num:
            max_num = img_num

    # Add 1 for the next image filename
    return max_num + 1

def getLastImageNum(folder=""):
    """
        Gets the number of the last image
    """
    import os
    
    images_list = os.listdir(IMAGES_DIR + folder)

    # If there are no existing images, return 1
    if len(images_list) == 0:
        return 1
    
    # Do not count previews folder
    if folder == "":
        images_list.remove('previews')
        images_list.remove('print_previews')
        
    # Get the maximum number from string numbers
    max_num = int(images_list[0][:-4])
    for i in images_list:
        # First convert string into int
        img_num = int(i[:-4])

        # Compare
        if img_num > max_num:
            max_num = img_num

    # Get the last image's number
    return max_num

def sendEmail(email):
    """
        Use SendGrid API to send email using local server
        Add email subject, body, attachments
    """
    sg = sendgrid.SendGridAPIClient(apikey='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')
    from_email = Email("photos@theassembly.ae")
    to_email = Email(email)
    subject = "Greetings from The Assembly @ GITEX 2018"
    content = Content("text/plain", "Thank you for visiting the Assembly at GITEX 2018! We hope you enjoyed all the fun exhibits at our stand as well as our smart photobooth. Your pictures are attached to this mail.\nIf you'd like to learn how to build things using smart technology, join us for our free weekly workshops at in5 Tech in Knowledge Village, Dubai every week! Sign up at http://members.theassembly.ae for more info and to RSVP.\nEnjoy your pictures, feel free to share them on social media and tag us at @MakeSmartThings on Facebook/Twitter/Instagram - \nBest regards,\nThe Assembly Team")

    mail = Mail(from_email, subject, to_email, content)

    # Get the last 4 taken picture
    file_path = IMAGES_DIR + "previews/" + str(getLastImageNum(folder='previews')) + ".png"

    for i in range(4):
        # Get the last taken picture
        file_path = IMAGES_DIR + "previews/" + str(getLastImageNum(folder='previews') - i) + ".png"

        with open(file_path, 'rb') as f:
            data = f.read()
            f.close()

        encoded = base64.b64encode(data).decode()

        attachment = Attachment()
        attachment.content = encoded
        attachment.type = "image/png"
        attachment.filename = "AssemblyPhotobooth" + str(i + 1) + ".png"
        attachment.disposition = "attachment"
        attachment.content_id = "Assembly"
        
        mail.add_attachment(attachment)


    try:
        response = sg.client.mail.send.post(request_body=mail.get())
    except urllib.HTTPError as e:
        print(e.read())
        exit()

def validateEmail(email):
    # Import regex library for email validation
    import re

    if not re.match(r"[^@]+@[^@]+\.[^@]+", email):
        return False
    else:
        return True

def checkRowExists(email):
     # Connect to DB
    db = pymysql.connect("localhost", "root", "password", "assembly-photobooth")
    cursor = db.cursor()

    cursor.execute(
        "SELECT email_id FROM user_data WHERE email_id = %s;",
        (email,)
    )

    # gets the number of rows affected by the command executed
    row_count = cursor.rowcount

    if row_count == 0:
        return False
    else:
        return True

def insertDataToDb(image_location, usage_type, email_id="", twitter_account=""):
    # Connect to DB
    db = pymysql.connect("localhost", "root", "password", "assembly-photobooth")

    cursor = db.cursor()
    sql = "INSERT INTO user_data (email_id, image_location, type, twitter_account) VALUES ('" + email_id + "', '" + image_location + "', '" + usage_type + "', '" + twitter_account + "');"
    cursor.execute(sql)
    db.commit()

    cursor.close()
    db.close()

def video_stream():
    global video_camera 
    global global_frame
    global current_filter

    if video_camera is None:
        video_camera = VideoCamera()
        
    while True:
        frame = video_camera.get_frame(filter=current_filter)

        if frame != None:
            global_frame = frame
            yield (b'--frame\r\n'
                    b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')
        else:
            yield (b'--frame\r\n'
                            b'Content-Type: image/jpeg\r\n\r\n' + global_frame + b'\r\n\r\n')

if __name__ == '__main__':
    app.run(host='0.0.0.0', threaded=True)