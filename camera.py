from thermalprinter import *
from PIL import Image
import numpy as np
import cv2

class VideoCamera(object):

    def __init__(self):
        self.cap = cv2.VideoCapture(0)

    def __del__(self):
        self.cap.release()

    def get_frame(self, filter='default'):
        ret, frame = self.cap.read()
        self.current_filter = None

        if ret:
            if filter == 'default':
                self.current_filter = frame
            elif filter == 'sketch':
                self.current_filter = self.sketch_filter(frame)
            elif filter == 'gaussian':
                self.current_filter = self.gaussian_filter(frame)
            elif filter == 'adaptive':
                self.current_filter = self.adaptivemean_filter(frame)
            elif filter == 'pencil':
                self.current_filter = self.pencil_filter(frame)
            elif filter == 'normalized':
                self.current_filter = self.normalized_filter_edge(frame)
            elif filter == 'recursive':
                self.current_filter = self.recursive_filter_edge(frame)
            elif filter == 'detail':
                self.current_filter = self.detail_enhance_filter(frame)
            elif filter == 'stylization':
                self.current_filter = self.stylization_filter(frame)
            elif filter == 'stylization_bw':
                self.current_filter = self.stylization_bw_filter(frame)
            elif filter == 'canny':
                self.current_filter = self.canny_filter(frame)
            elif filter == 'old_sketch':
                self.current_filter = self.old_sketch_filter(frame)
            elif filter == 'sepia':
                self.current_filter = self.sepia_filter(frame)

            filtered_img = self.current_filter
            ret, png = cv2.imencode('.png', filtered_img)
            return png.tobytes()
        else:
            return

    def save_png(self, filename, filter=""):
        ret, frame = self.cap.read()
        img_name = str(filename) + '.png'
        if ret:
            filtered_img = None

            if filter == "":
                filtered_img = self.addBorder(self.current_filter)
            elif filter == "gaussian":
                filtered_img = self.gaussian_filter(self.addBorder(frame, bw=True))
            elif filter == "stylized":
                add_stylization = self.stylization_filter(frame)
                filtered_img = self.addBorder(add_stylization)
            elif filter == "sketch":
                filtered_img = self.sketch_filter(self.addBorder(frame, bw=True))
                
            cv2.imwrite('static/images/' + img_name, filtered_img)

            return 'Picture taken'
        else:
            return 'Failed to take picture'

    def addLogo(self, img):
        logo_img = cv2.imread('static/icons/logo_black.png')
        logo_img = cv2.cvtColor(logo_img, cv2.COLOR_BGR2GRAY)
        x_offset = img.shape[1] - logo_img.shape[1] - 5
        y_offset = 5
        img[y_offset:y_offset + logo_img.shape[0], x_offset:x_offset + logo_img.shape[1]] = logo_img
        return img

    def addBorder(self, filtered_img, color_filter=None, bw=False):
        if bw is False:
            white = cv2.imread('static/icons/white_mask.png')
            white1 = cv2.imread('static/icons/white_mask.png')
            word = cv2.imread('static/icons/assembly_logo.png')
            word2 = cv2.imread('static/icons/assembly_logo_blank.jpg')
            logo = cv2.imread('static/icons/assembly_new_sign.png')
        elif bw is True:
            white = cv2.imread('static/icons/white_mask.png')
            white1 = cv2.imread('static/icons/white_mask.png')
            word = cv2.imread('static/icons/assembly_logo_black.png')
            word2 = cv2.imread('static/icons/assembly_logo_blank_black.jpg')
            logo = cv2.imread('static/icons/assembly_new_sign_black.png')

        if color_filter is not None:
            filtered_img = color_filter

        white = cv2.resize(white,None,fx=(filtered_img.shape[1]/640), fy=(filtered_img.shape[0]/480), interpolation = cv2.INTER_CUBIC)
        white1 = cv2.resize(white1,None,fx=(filtered_img.shape[1]/640), fy=(filtered_img.shape[0]/480), interpolation = cv2.INTER_CUBIC)
        logo = cv2.resize(logo,None,fx=0.7, fy=0.7, interpolation = cv2.INTER_CUBIC)
        word = cv2.resize(word,None,fx=0.6, fy=0.6, interpolation = cv2.INTER_CUBIC)
        word2 = cv2.resize(word2,None,fx=0.6, fy=0.6, interpolation = cv2.INTER_CUBIC)
        x_offset= white.shape[1] - 110
        y_offset= 30
        white[y_offset:y_offset+logo.shape[0], x_offset:x_offset+logo.shape[1]] = logo
        x_offset1= 0
        y_offset1= white.shape[0] - 90
        white[y_offset1:y_offset1+word.shape[0], x_offset1:x_offset1+word.shape[1]] = word
        
        if bw is True:
            pass
            # cv2.rectangle(white,(0,0),(white.shape[1],white.shape[0]),(0, 0, 0),20)
        elif bw is False:
            cv2.rectangle(white,(0,0),(white.shape[1],white.shape[0]),(38,35,235),20)
        
        white2 = white.copy()
        white[y_offset1:y_offset1+word.shape[0], x_offset1:x_offset1+word.shape[1]] = word2
        white_gray = cv2.cvtColor(white,cv2.COLOR_BGR2GRAY)
        ret, mask = cv2.threshold(white_gray, 130, 255, cv2.THRESH_BINARY)
        mask_inv = cv2.bitwise_not(mask)

        im1 = cv2.bitwise_and(filtered_img,filtered_img,mask=mask)
        im2 = cv2.bitwise_and(white2,white2,mask= mask_inv)

        res = cv2.add(im1,im2)
        return res

    def sepia_filter(self, img):
        newImage = img
        i, j, k = img.shape
        
        for x in range(i):
            for y in range(j):
                R = img[x,y,2] * 0.393 + img[x,y,1] * 0.769 + img[x,y,0] * 0.189
                G = img[x,y,2] * 0.349 + img[x,y,1] * 0.686 + img[x,y,0] * 0.168
                B = img[x,y,2] * 0.272 + img[x,y,1] * 0.534 + img[x,y,0] * 0.131
                
                if R > 255:
                    newImage[x,y,2] = 255
                else:
                    newImage[x,y,2] = R
                
                if G > 255:
                    newImage[x,y,1] = 255
                else:
                    newImage[x,y,1] = G
                
                if B > 255:
                    newImage[x,y,0] = 255
                else:
                    newImage[x,y,0] = B

        return newImage

    def canny_filter(self, img):
        grayscale = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        bil = cv2.bilateralFilter(grayscale, 5, 10, 10)
        edges = cv2.Canny(bil, 0, 35)
        blank = np.ones((480, 640)) * 255
        res = blank - edges

        return res

    def pencil_filter(self, img):
        dst_gray, dst_color = cv2.pencilSketch(img, sigma_s=60, sigma_r=0.07, shade_factor=0.05)
        return dst_color

    def normalized_filter_edge(self, img):
        filter = cv2.edgePreservingFilter(img, flags=2, sigma_s=60, sigma_r=0.4)
        return filter

    def recursive_filter_edge(self, img):
        filter = cv2.edgePreservingFilter(img, flags=1, sigma_s=60, sigma_r=0.4)
        return filter

    def detail_enhance_filter(self, img):
        filter = cv2.detailEnhance(img, sigma_s=10, sigma_r=0.15)
        return filter

    def stylization_filter(self, img):
        filter = cv2.stylization(img, sigma_s=60, sigma_r=0.07)
        return filter

    def stylization_bw_filter(self, img):
        filter = cv2.stylization(img, sigma_s=60, sigma_r=0.07)
        filter = cv2.cvtColor(filter, cv2.COLOR_BGR2GRAY)
        return filter

    def gaussian_filter(self, img):
        img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img_gray = cv2.medianBlur(img_gray, 1)
        img_gray2 = img_gray + 13
        img_gray_inv = 255 - img_gray2
        img_blur = cv2.GaussianBlur(img_gray_inv, ksize=(21, 21), sigmaX=0,
          sigmaY=0)

        def dodgeV2(image, mask):
            return cv2.divide(image, 255 - mask, scale=256)

        def burnV2(image, mask):
            return 255 - (cv2.divide(255 - image, 255 - mask, scale=256))

        img_blend = dodgeV2(img_gray, img_blur)
        thresh, filtered_img = cv2.threshold(img_blend, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
        
        return filtered_img

    def adaptivemean_filter(self, img):
        img1 = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        bil = cv2.bilateralFilter(img1, 9, 20, 20)
        filtered_img = cv2.adaptiveThreshold(bil, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 11, 2)
        return filtered_img

    def sketch_filter(self, img):
        im = cv2.cvtColor(img,cv2.COLOR_BGR2YCrCb)
        gray = cv2.equalizeHist(im[0])
        gray = cv2.equalizeHist(im[3])
        gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
        gray = cv2.equalizeHist(gray)
        clahe = cv2.createCLAHE(clipLimit=3.0, tileGridSize=(10,10))
        gray = clahe.apply(gray)
        blur = cv2.GaussianBlur(gray,(5,5),0)
        sobelx = cv2.Sobel(gray,cv2.CV_64F,1,0,ksize=1)
        abs_sobelx = cv2.convertScaleAbs(sobelx)
        sobely = cv2.Sobel(gray,cv2.CV_64F,0,1,ksize=1)
        abs_sobely = cv2.convertScaleAbs(sobely)
        res = cv2.addWeighted(abs_sobelx,0.5,abs_sobely,0.5,0)
        res_norm = np.zeros_like(res)
        res_norm = cv2.normalize(res,res_norm,0,255,cv2.NORM_MINMAX)
        final = cv2.bilateralFilter(res_norm,3,15,15)
        # final = cv2.adaptiveThreshold(final, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 11, 2)
        inv = cv2.bitwise_not(final)

        return inv

    def old_sketch_filter(self, img):
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        bil = cv2.bilateralFilter(img, 4, 20, 20)
        filtered_img = cv2.adaptiveThreshold(bil, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)

        return filtered_img

    def getLastImageNum(self, folder=""):
        """
            Gets the number of the last image
        """
        import os
        
        images_list = os.listdir("static/images/" + folder)

        # If there are no existing images, return 1
        if len(images_list) == 0:
            return 1
        
        # Do not count previews folder
        if folder == "":
            images_list.remove('previews')
            images_list.remove('print_previews')
            
        # Get the maximum number from string numbers
        max_num = int(images_list[0][:-4])
        for i in images_list:
            # First convert string into int
            img_num = int(i[:-4])

            # Compare
            if img_num > max_num:
                max_num = img_num

        # Get the last image's number
        return max_num

    def print_img(self, img_loc):
        with ThermalPrinter(port='/dev/ttyUSB0') as printer:
            base_img = cv2.imread(img_loc)

            # Resize image
            resized_img = cv2.resize(base_img, None, fx=0.8, fy=0.8, interpolation=cv2.INTER_CUBIC)

            # Save the resized image
            cv2.imwrite("static/images/" + str(self.getLastImageNum()) + ".png", resized_img)

            # Rewrite print preview image with border
            img_with_border = self.addBorder(resized_img)
            cv2.imwrite(img_loc, img_with_border)

            # Open the image again
            img = Image.open(img_loc)
            
            # Rotate image
            img = img.rotate(270, expand=True)

            printer.image(img)
            printer.feed(2)